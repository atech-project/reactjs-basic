import React from "react";
import {render} from "react-dom";

class App extends React.Component{
    render(){
        return(
            <div className="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/>
              <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
              <button type="submit" className="btn btn-primary">Submit</button>
            </div>

        );
    }
}

render(<App/>, window.document.getElementById("app"));

